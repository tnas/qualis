
def carrega(arquivo, entradas):
    '''
    Carrega os dados no arquivo txt (extraidos do PDF) em uma lista
    '''
    confs = []
    n = len(entradas)
    with open(arquivo, 'rb') as conf_file:
        cont = 0
        for linha in conf_file:
            info = linha.strip()
            chave = entradas[cont % n]
            if cont % n == 0:
                confs.append({chave: info})
            else:
                confs[cont//n][chave] = info
            cont += 1
    return confs

def carrega_confs():
    return carrega('dados/qualisConf.txt', ['sigla', 'nome', 'indice_h', 'estrato'])

def carrega_periods():
    return carrega('dados/qualisPeriod.txt', ['issn', 'titulo', 'estrato', 'area_de_avaliacao', 'classificacao'])

if __name__=='__main__':
    confs = carrega_confs()
    print len(confs)
    periods = carrega_periods()
    print len(periods)